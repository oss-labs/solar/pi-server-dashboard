FLUTTER_SDK=/Users/chris/development/flutter
flutter build bundle
$FLUTTER_SDK/bin/cache/dart-sdk/bin/dart \
$FLUTTER_SDK/bin/cache/dart-sdk/bin/snapshots/frontend_server.dart.snapshot \
--sdk-root $FLUTTER_SDK/bin/cache/artifacts/engine/common/flutter_patched_sdk_product \
--target=flutter \
--aot \
--tfa \
-Ddart.vm.product=true \
--packages .dart_tool/package_config.json \
--output-dill build/kernel_snapshot.dill \
--verbose \
--depfile build/kernel_snapshot.d \
package:pi_server_dashboard/main.dart